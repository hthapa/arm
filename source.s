.global main
.func main
 
main:
	ldr r1, addr_lr @ Getting the address for addr_lr
	str lr, [r1]	@ Storing link r1

	ldr r1, addr_numbers  @ Getting the address for addr_numbers
	vldr s0, [r1]  		  @ Moving value into s0 register
	vcvt.f64.f32 d1, s0   @ Converting float and integers
	ldr r0, =output		  @ Poiting to the output
	vmov r2,r3, d1
	bl printf			@ Using the C printf commmand to display the text


	mov r2,#0x5  @The number of characters output
	mov r5,r2
	b loop_1  	@enters the label loop

loop_1:
	mov r1,pc   @it Points owards  the first position of the string
	ldr r1,=msg	@load the string of msg in r1
	sub r5,r5,#0x1
	cmp r5,#0x0  @compairing r5 with 0, if equals, braches to
                    @R4 is compared to 0, and if r5  is equal to 0, branches to label exit
	beq exit
	mov r4,#0x0       	@moving value 0 into register r5
	b loop_2 	@branchs to another loop
	b loop_1 	@repeats the same loop over and again
 
loop_2:
	ldrb r3,[r1]	@loads the byte of register r1 to the r3
	ldrb r6,[r1,#0x1]  @loads the byte and update the address as well
	cmp r3,r6   	@compares r3 with r6
	strgtb r3,[r1,#0x1]   @the value of r3 if greater than r6 the values are swapped.
	strgtb r6,[r1]
	add r1,r1,#0x1    	@Rregister r1 now points towards another number
	add r4,r4,#0x1     	@Similarly, r4 points towards another one as well
	cmp r4,r5   	@now the vlaue of  r4 and r5 are compared
	bne loop_2   	@ branches towards label loop_2  if r4 and r4 is not equal
	b loop_1      	@if they are equal jump off the loop_2 and gain cycles towards loop_1
                    @first cycle
exit:
	add r2,r2,#0x1
	mov r0,#0x1   @writing the msg to stdout

	mov r1,pc	
	ldr r1,=msg
	mov r7,#0x4
	swi 0
 
	mov r0,#0x0   @ telling the
	mov r7,#0x1  @ @set r7 to 1 which is the syscall for exit
	
	ldr lr, addr_lr @ Restoring link register
	ldr lr, [lr]
	bx lr

addr_lr: .word bu
addr_text: .word text
addr_numbers: .word numbers

.data
msg: .ascii "51428"
number: .word 0
bu: .word 0
text: .asciz " is the sorted numbers"
numbers: .float 5.0
output: .asciz "Sorting %f numbers: \n"

.global printf